import pkg_resources

version = pkg_resources.require("taskr-cli")[0].version
