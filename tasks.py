from importlib.metadata import version
from typing import Optional

from taskr import runners, utils

PACKAGE_NAME = "taskr_cli"
DEFAULT = "reinstall"
IMAGE_NAME = "taskr_image"


# Builds the wheel
def build() -> bool:
    return runners.run(
        ["python -m build --wheel -n > /dev/null;", "echo 'Artifact:'; ls dist/"]
    )


# Remove build artifacts, cache, etc.
def clean() -> bool:
    ret = utils.cleanBuilds()
    ret2 = utils.cleanCompiles()
    return all([ret, ret2])


# Re-installs taskr
def reinstall() -> bool:
    return runners.run_conditional(
        clean,
        lambda: runners.run(f"pip uninstall {PACKAGE_NAME} --y"),
        build,
        lambda: runners.run(f"pip install dist/{PACKAGE_NAME}-*"),
        clean,
    )


# Build test image
def docker() -> bool:
    return runners.run(f"podman build . -t {IMAGE_NAME} ")


# Bump setup.py version
def bump(version: Optional[str] = None) -> bool:
    return utils.bumpVersion(version, filename="pyproject.toml")


# Run tests
def test(subset: Optional[str] = None) -> bool:
    cmd = "python -m pytest tests/ -v "
    if subset:
        cmd += f"-k {subset}"

    return runners.run(cmd)


# Run black
def fmt() -> bool:
    return runners.run("python -m black .")


# Checks types
def mypy() -> bool:
    return runners.run("python -m mypy taskr/*.py ")


def lint() -> bool:
    return runners.run("ruff tasks.py tests/* taskr/* --fix")


# Runs all static analysis tools
def check() -> bool:
    return runners.run_conditional(fmt, lint, mypy)


# Send it!
def upload_test() -> bool:
    return runners.run("python -m twine upload --repository testpypi dist/*")


# Send it for real!
def upload_prod() -> bool:
    return runners.run("python -m twine upload dist/*")


# Squash the branch
def squish() -> None:
    runners.run("git rebase -i `git merge-base HEAD master`")


# Tag the branch
def tag(ver: str = "") -> None:
    if ver == "":
        ver = version("taskr-cli")

    runners.run([f"git tag v{ver};", "git push --tags"])


"""
Various tasks to test package functions
"""


# Test defaults
def test_env(test: str = "one", test2: str = "two") -> bool:
    ENV = {"a": "b"}
    return runners.run(f"echo '{test}'", ENV)


def print_env() -> None:
    runners.run("env | sort")


def mod() -> bool:
    return utils.addTaskrToEnv()


def key(two: str, one: str = "one") -> None:
    print(one)
    print(two)
