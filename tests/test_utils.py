import os
import shutil

import pytest

from taskr.utils import (
    _bump,
    _removeFolder,
    bumpVersion,
    cleanCompiles,
    inVenv,
    readEnvFile,
)

"""
Untested features
    - Adding env var to venv activation file
"""

"""
 Ensure our rmtree wrapper works and doesn't fail without files
"""


def test_remove_build():
    path = "./tests/fakeFolders/build"
    os.makedirs(path)
    assert os.path.exists(path)
    _removeFolder(path="build", root="./tests/fakeFolders")
    assert not os.path.exists(path)


def test_remove_build_empty():
    path = "./tests/fakeFolders/build"
    assert not os.path.exists(path)
    _removeFolder(path="build", root="./tests/fakeFolders")
    assert not os.path.exists(path)


"""
# Can we remove compiled files and folders, nested and not
"""


def test_remove_compiles():
    path = "./tests/fakeFolders/temp/__pycache__"
    path3 = "./tests/fakeFolders/temp/__pycache__/comp.pyc"
    path2 = "./tests/fakeFolders/__pycache__"
    path4 = "./tests/fakeFolders/comp.pyc"

    os.makedirs(path)
    os.makedirs(path2)
    open(path3, 'a')
    open(path4, 'a')

    assert os.path.exists(path)
    assert os.path.exists(path2)
    assert os.path.exists(path3)
    assert os.path.exists(path4)

    cleanCompiles()
    assert not os.path.exists(path)
    assert not os.path.exists(path2)
    assert not os.path.exists(path3)
    assert not os.path.exists(path4)


def test_remove_compiles_empty():
    assert cleanCompiles()


"""
Misc util functions
"""


def test_in_venv():
    os.environ["VITRUAL_ENV"] = "dummy"
    if not os.getenv("CI"):
        assert inVenv()


def test_read_env_file():
    file = "./tests/data/test.env"
    ret = readEnvFile(file)
    assert ret == {"a": "1", "b": "2", "c": "3", "d": "4"}


def test_read_env_file_not_found():
    # Should fail with a print, no exception
    file = "./tests/data/test_not_exists.env"
    readEnvFile(file)


# bump logic
def test_bump_logic():
    assert "1.1.2" == _bump("1.1.1")
    assert "0.0.1" == _bump("0.0.0")
    assert "1.1.10" == _bump("1.1.9")
    assert "1.1.10" == _bump("1.1.09")
    assert "1.1.10" == _bump("1.1.09.dev0")


def test_bump_logic_invalid():
    with pytest.raises(Exception):
        _bump("0.1.1a")


def _check_file(filename, toCheck):
    """
    Check our setup files to make sure they were edited
    """
    with open(filename, "r") as fd:
        temp = fd.read()

    hasIt = False

    for line in temp.split("\n"):
        if line == toCheck:
            hasIt = True

    return hasIt


def _setup_file(newFile):
    template = './tests/files/setup_template.py'

    if os.path.exists(newFile):
        os.remove(newFile)

    shutil.copyfile(template, newFile)


def test_bump_files():
    newFile = './tests/files/setup_default.py'

    _setup_file(newFile)
    bumpVersion(filename=newFile)
    assert _check_file(newFile, 'version="1.0.1",')

    _setup_file(newFile)
    bumpVersion(filename=newFile, variable="versiona")
    assert _check_file(newFile, 'versiona="1.0.1"')

    _setup_file(newFile)
    bumpVersion(filename=newFile, variable="versionb")
    assert _check_file(newFile, 'versionb = "1.0.1"')

    _setup_file(newFile)
    bumpVersion(filename=newFile, variable="versionc", version="2.0.0")
    assert _check_file(newFile, 'versionc = "2.0.0"')

    _setup_file(newFile)
    bumpVersion(filename=newFile, variable="__version__")
    assert _check_file(newFile, '__version__ = "1.0.1"')
