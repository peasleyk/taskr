import pytest

from taskr.runners import run, run_conditional, run_output

"""
Untested features
    - env file at top of tasks.py
    - venv_required flag set to true

Ensure run returns what we want
"""


def test_run_pass():
    """
    We can run shell commands
    """

    command = "echo 'ran'"
    assert run(command) is True


def test_run_pass_list():
    """
    We can run a list of  shell commands
    """
    command = ["ls", "-l", "-h", "|", "wc", "-l"]
    assert run(command) is True


def test_run_fail():
    """
    Commands that don't exist fail
    """

    command = "not_a_callable"
    assert run(command) is False


def test_run_unsupported():
    """
    Non strings fail
    """
    command = True
    with pytest.raises(Exception, match="Unsupported command type:"):
        run(command) is False


"""
Ensure ENV works and that it actually adds ENV variables
"""


def test_run_env():
    """
    We can run shell commands and pass environment variables to subprocess
    """

    command = "env | wc -l"
    env = {"test": "True", "test1": "True"}
    assert run(command, env) is True


def test_run_env2():
    """
    ENv passing works, and we get different outputs
    """
    command = "env | wc -l"
    env = {"test": "True", "test2": "True"}
    ret = run_output(command, env=env)
    ret1 = run_output(command)

    assert int(ret1.stdout) < int(ret.stdout)


"""
Ensure we can get output
"""


def test_run_output_pass():
    """
    We get valid output for a list command
    """
    command = ["ls", "-l", "|", "wc", "-l", "|", "wc", "-l"]
    ret = run_output(command)
    assert ret.stdout == "1"
    assert ret.stderr == ""
    assert ret.status is True


def test_run_output_env_pass():
    """
    We get valid output with environment var passed
    """

    command = "env | grep 'custom_env' | wc -l"
    env = {"custom_env": "taskr"}
    ret = run_output(command, env=env)
    assert ret.stdout == "1"
    assert ret.stderr == ""
    assert ret.status is True


def test_run_output_list():
    """
    String command gets output
    """
    command = "echo 'hello'"
    ret = run_output(command)
    assert ret.stdout == "hello"
    assert ret.stderr == ""
    assert ret.status is True


def test_run_output_fail():
    """
    Bad callable gets a valid output
    """

    command = "not_a_callable"
    ret = run_output(command)
    assert ret.stdout == ""
    assert ret.stderr != ""
    assert ret.status is False


"""
Ensure conditional run behaves
"""


def test_conditional():
    """
    Correct tasks run all the way through
    """

    def one():
        return True

    def two():
        return True

    def three():
        return True

    assert run_conditional(one, two, three, print) is True


def test_conditional_non_boolean_return():
    import math

    assert run_conditional(math.pow(3, 3)) is False


def test_conditional_bail(capsys):
    """
    Conditional output will stop early if it fails
    """

    def one():
        return True

    def two():
        return False

    def three():
        return True

    ret = run_conditional(one, two, three)
    out, _ = capsys.readouterr()
    assert "Task two failed, bailing" in out
    assert ret is False


def test_conditional_unsuported(capsys):
    """
    Non callables will fail
    """

    def one():
        return True

    def two():
        return True

    ret = run_conditional(one, 1, two)
    out, _ = capsys.readouterr()
    assert "is not callable" in out
    assert ret is False


def test_nested_conditional():
    """
    We can run nested taskr.run* functions if we want
    """

    def one():
        return True

    def two():
        return True

    def cond():
        return run_conditional(one, two)

    def three():
        return True

    def four():
        return True

    ret = run_conditional(cond, three, four)
    assert ret is True
