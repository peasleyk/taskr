import pytest

from taskr.taskr import _Function, _Taskr

"""
Untested features
    - env file at top of tasks.py, ensure these are passed to function
    - venv_required flag set to true
    - PYENV_DIR or TASKR_DIR set

Ensure we can handle defaults
"""


def test_default():
    t = _Taskr(None)
    t.default_function = "test"
    assert t.hasDefault()


def test_default_empty():
    t = _Taskr(None)
    assert t.hasDefault() is False


def test_default_no_func():
    t = _Taskr(None)
    t.default_function = "test"
    t.default()


"""
Ensure we list tasks without failing
"""


def test_list(capsys):
    t = _Taskr(None)
    t.funcs = {
        "abs": _Function("abs", abs),
    }

    t.list()
    out, _ = capsys.readouterr()
    ret = (
        "\nTasks and arguments:\n abs : "
        "Return the absolute value of the argument.\n\n* = default\n"
    )
    assert out == ret


def test_empty_list(capsys):
    t = _Taskr(None)
    t.list()

    out, _ = capsys.readouterr()
    ret = "No tasks defined or found in tasks.py\n"
    assert out == ret


"""
Ensure we can run functions (tasks) correctly
"""


def test_process():
    t = _Taskr(None)

    def test_func():
        return 1

    t.funcs = {"test": test_func}

    t.process("test")


def test_process_fail(capsys):
    t = _Taskr(None)
    t.process("test")
    out, _ = capsys.readouterr()
    assert out.strip() == "Unknown task: test"


def test_process_assert(capsys):
    def test_func():
        return 1 / 0

    t = _Taskr(None)
    t.funcs = {
        "test": _Function(name="test", func=test_func),
    }

    t.process("test")
    out, _ = capsys.readouterr()
    assert out.strip() == "Error running task test: division by zero"


"""
Test args
"""


def test_arguments_correct(capsys):
    # Mock a function where we have a default value of two
    def test_func(num: int = 2) -> None:
        print(num / 2)

    t = _Taskr(None)
    t.funcs = {
        "test": _Function(
            name="test_func", func=test_func, defaults=[2], argnames=['num']
        ),
    }

    t.process("test")
    out, _ = capsys.readouterr()
    assert out.strip() == "1.0"

    t.process("test", [2])
    out, _ = capsys.readouterr()
    assert out.strip() == "1.0"

    t.process("test", [4])
    out, _ = capsys.readouterr()
    assert out.strip() == "2.0"


def test_multiple_arguments_correct(capsys):
    def test_func(num: int = 2, demon: int = 4) -> None:
        print(num / demon)

    t = _Taskr(None)
    t.funcs = {
        "test": _Function(
            name="test_arg", func=test_func, defaults=[2, 4], argnames=['num', 'demon']
        ),
    }

    t.process("test", [2])
    out, _ = capsys.readouterr()
    assert out.strip() == "0.5"

    t.process("test", [4])
    out, _ = capsys.readouterr()
    assert out.strip() == "1.0"

    t.process("test", [4, 1])
    out, _ = capsys.readouterr()
    assert out.strip() == "4.0"


def test_arguments_too_many(capsys):
    def test_func(num: int = 2) -> None:
        print(num / 2)

    t = _Taskr(None)
    t.funcs = {
        "test": _Function(
            name="test_arg", func=test_func, defaults=[2], argnames=['num']
        ),
    }

    t.process("test", [2, 3])
    out, _ = capsys.readouterr()
    assert out.strip() == "Warning - More arguments passed than task takes. Skipping"


def test_arguments_default_no_default(capsys):
    """
    We should be able to pass arguments to a function with both defaults and
    non defaults
    """

    def test_func(num2: int, num: int = 2) -> None:
        print(num / 2)

    t = _Taskr(None)
    t.funcs = {
        "test": _Function(
            name="test_arg", func=test_func, defaults=[2], argnames=['num2', 'num']
        ),
    }

    t.process("test", [2, 3])
    out, _ = capsys.readouterr()
    assert out.strip() != "Warning - More arguments passed than task takes. Skipping"


def test_arguments_ignore(capsys):
    """
    We passed an argument to a function that doesn't take one, ignore it
    """

    def test_func():
        print(2)

    t = _Taskr(None)
    t.funcs = {
        "test": _Function(name="test_arg", func=test_func),
    }

    t.process("test", [2])
    out, _ = capsys.readouterr()
    assert out.strip() == "2"


def test_kwargs_logic():
    t = _Taskr(None)

    # Empty should return empty
    test_args = []
    args, kwagrs = t._grabKwargs(test_args)
    assert args == []
    assert kwagrs == {}

    # Mix of kwargs and normal args
    test_args = [2, 3, "a", "b=3", "f= 3"]
    args, kwagrs = t._grabKwargs(test_args)
    assert args == [2, 3, "a"]
    assert kwagrs == {"b": "3", "f": "3"}

    # Test a list for fun
    test_args = [[1, 3, "a"]]
    args, kwagrs = t._grabKwargs(test_args)
    assert args == [[1, 3, "a"]]
    assert kwagrs == {}

    # This should be a diff failure - no exception
    test_args = ["b==3"]
    with pytest.raises(Exception):
        _, kwagrs = t._grabKwargs(test_args)
