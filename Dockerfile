FROM python:3.10-slim

WORKDIR /usr/app

COPY . /usr/app

ENV CI=true

RUN pip --disable-pip-version-check  install pipenv taskr-cli   && \
    pipenv install --dev --system --skip-lock